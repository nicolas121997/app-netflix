package co.samtel.appnetflix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy // actúa como proxy zuul. 
@EnableEurekaServer
public class AppNetflixApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppNetflixApplication.class, args);
	}

}
